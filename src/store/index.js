import Vue from 'vue';
import Vuex from 'vuex';
//Import global actions
import * as actions from './actions';
//Import global mutations
import * as mutations from './mutations';
//Import global getters
import * as getters from './getters';
import restaurants from './modules/restaurants';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  getters,
  actions,
  mutations,
  modules: {
  },
  modules: {
    restaurants
  }
});
