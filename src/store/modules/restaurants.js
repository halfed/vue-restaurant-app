import { getRestaurants } from '@/api/allRestaurants.api.js';

const setUpPagination = (length, numPerPage) => {
  let pageCount = Math.ceil(length / numPerPage);
  return Array.from(Array(pageCount), (_, i) => i + 1);
}

const alphabetizeList = (list) => {
  let newList = list.sort(function (a, b) {
    if (a.name < b.name) { return -1; }
    if (a.name > b.name) { return 1; }
    return 0;
  });
  return newList;
}

const state = {
  restaurants: [],
  updatedRestaurants: [],
  restaurantsPerPage: 10,
  numberOfPages: [],
  currentPage: 1
}

const mutations = {
  POPULATE_RESTAURANTS(state, payload) {
    state.restaurants = payload;
    state.updatedRestaurants = alphabetizeList(payload);

    state.numberOfPages = setUpPagination(payload.length, state.restaurantsPerPage); //Array.from(Array(pageCount), (_, i) => i + 1);
  },
  CHANGE_CURRENT_PAGE(state, data) {
    state.currentPage = data;
  },
  UPDATE_CURRENT_RESTAURANT_LIST(state, payload) {
    state.updatedRestaurants = alphabetizeList(payload);
    state.numberOfPages = setUpPagination(payload.length, state.restaurantsPerPage);//Array.from(Array(pageCount), (_, i) => i + 1);
  }
}

const actions = {
  getAllRestaurants({ commit }) {
    return getRestaurants()
      .then(restaurantData => {
        commit('POPULATE_RESTAURANTS', restaurantData.data);
      });
  },
  getResults({ commit }, { searchChecker }) {
    commit('UPDATE_CURRENT_RESTAURANT_LIST', searchChecker)
  }
}

const getters = {
  getAllRestaurants() {
    return state.restaurants;
  },
  getAllUpdatedRestaurants() {
    return state.updatedRestaurants;
  },
  getNumberOfPages() {
    return state.numberOfPages;
  },
  getRestaurantCount() {
    return state.restaurantsPerPage;
  },
  getCurrentPage() {
    return state.currentPage;
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
