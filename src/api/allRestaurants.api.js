import httpClient from './httpClient';

const END_POINT = '/restaurants';


const getRestaurants = () => httpClient.get(END_POINT);
//console.log("getResTaurants: " + getRestaurants);

export {
  getRestaurants
}
