import axios from 'axios';

const httpClient = axios.create({
  baseURL: 'https://code-challenge.spectrumtoolbox.com/api',
  headers: {
    "Content-Type": "application/json",
    // anything you want to add to the headers
    Authorization: "Api-Key q3MNxtfep8Gt"
  }
});

export default httpClient;
