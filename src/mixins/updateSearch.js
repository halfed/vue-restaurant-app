export const updateSearch = {
  data() {
    return {
      genre: "",
      state: "",
      search: ""
    }
  },
  methods: {
    updateList(searchType, searchAgainstA, searchAgainstB) {
      let searchChecker = [];
      let restaurantList = [];

      if (this.searchAgainstB !== "" || this.searchAgainstA !== "") {
        restaurantList = this.$store.getters.getAllUpdatedRestaurants;
      } else {
        restaurantList = this.$store.getters.getAllRestaurants;
      }
      if (/^[a-zA-Z]+$/.test(searchData)) {
        this.genre = searchData;
        searchChecker = restaurantList.filter(b => b.genre.includes(searchData));
        if (searchChecker.length) {
          this.$store.dispatch('getResults', { searchChecker });
          this.selectionList = true;
        } else {
          this.selectionList = false;
        }
      } else if (!/^[a-zA-Z]+$/.test(searchData) && searchData !== "") {
        this.selectionList = false;
      } else {
        this.filterType[searchType] = "";
        if (this.state !== "") {
          restaurantList = this.$store.getters.getAllRestaurants;
          searchChecker = restaurantList.filter(b => b.state.includes(this.state));
          this.$store.dispatch('getResults', { searchChecker });
        } else {
          this.$store.dispatch('getAllRestaurants');
        }
        this.selectionList = true;
      }
    }
  },
  created() {
    /*genreEventBus.$on('searchGenre', (searchData) => {
      this.updateList("genre", "state", "search");
    });*/
  }
}
