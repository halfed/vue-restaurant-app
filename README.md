<h1>Charter/Spectrum Front-End Code Challenge</h1>
<p>For this challenge we would like you to create a Vue application that pulls 
restaurant data from a simple API, displays that data in a table, and allows users 
to filter that data.</p>
<h3>API Endpoint: https://code-challenge.spectrumtoolbox.com/api/restaurants</h3>
<h3>API Key Header: Authorization | Api-Key q3MNxtfep8Gt</h3>
<code>
Example Fetch: fetch(�https://code-challenge.spectrumtoolbox.com/api/restaurants�, { 
	headers: { 
		Authorization: �Api-Key q3MNxtfep8Gt�, 
	}, 
}); 
</code>
<p>User stories are as follows:</p>
<ul>
	<li>A user should be able to see the name, city, state, phone number, and genres for each restaurant</li>
	<li>A user should see results sorted by name in alphabetical order starting with the beginning of the alphabet</li>
	<li>A user should be able to filter restaurants by state. If a state is selected that does not contain any restaurants, there should be something that indicates no restaurants were found for that state.</li>
	<li>A user should be able to filter by genre.</li>
	<li>State and Genre filters should default to �All�</li>
	<li>A user should be able to enter text into a search field. When hitting the enter key or clicking on a search button, the table should filter results. Search results should match either the name, city, or genre.</li>
	<li>A user should be able to clear the search filter by clearing the text value in the search input.</li>
	<li>A user should only see 10 results at a time and the table should be paginated.</li>
	<li>A user should be able to combine filters and search. The user should be able to turn filters on and off while a search value is present.</li>
</ul>
<p>What we are looking for:</p>
<ul>
	<li>No use of third party libraries for the table/filter/search. Using Nuxt.js as a starter kit is okay.</li>
	<li><b>FOR THIS EXERCISE WE WILL BE USING VUE.JS</b></li>
	<li>Well organized file structure</li>
	<li>Descriptive naming conventions</li>
	<li>DRY code that is readable and production ready</li>
	<li>Reusable components</li>
	<li>Sound logic for how the filters are architected</li>
	<li>Styling follows a convention/pattern and is well organized</li>
	<li>Full Git history with atomic commits</li>
</ul>
<p>Stretch goals:</p>
<ul>
	<li>Deployed application</li>
	<li>CI / CD</li>
	<li>Unit tests</li>
	<li>TypeScript</li>
	<li>Table row click shows additional information</li>
	<li>Add filter for attire</li>
	<li>Feel free to get creative!</li>
</ul>

